from django.shortcuts import render_to_response
from django.template.context import RequestContext
from django.http import HttpResponse
from registration.forms import UserProfileForm, UserForm
from django.contrib.auth import authenticate, login, logout
from django.http.response import HttpResponseRedirect
from django.contrib.auth.decorators import login_required
from .models import UserProfile

# Create your views here.
def register(request):
    context = RequestContext(request)
    registered = False

    if request.POST:
        form = UserForm(request.POST)
        profile_form = UserProfileForm(request.POST)

        if form.is_valid() and profile_form.is_valid():
            user = form.save()
            user.set_password(user.password)
            user.save()

            profile = profile_form.save(commit=False)
            profile.user = user          
            profile.save()
            registered = True
        else:
            form.errors, profile_form.errors

    else:
        form = UserForm()
        profile_form = UserProfileForm()

    return render_to_response('registration/register.html',{'form':form,'profile_form':profile_form,'registered':registered},context)


def user_login(request):
    context = RequestContext(request)
    
    if request.POST:
        username = request.POST['username']
        password = request.POST['password']
        
        user = authenticate(username=username, password=password)

        if user:
            if user.is_active:
                login(request,user)
                return HttpResponseRedirect('/registration/index/')
            else:
                return HttpResponse("Your registration account is disabled.")
        else:
            print "Invalid login details: {0}, {1}".format(username, password)
            return HttpResponse("Invalid login details supplied.")
    else:
        return render_to_response('registration/login.html', {}, context)



@login_required
def user_logout(request):
    # Since we know the user is logged in, we can now just log them out.
    logout(request)

    # Take the user back to the homepage.
    return HttpResponseRedirect('/registration/login/')


@login_required
def index(request):
    context = RequestContext(request)
    user = request.user
    user_profile = UserProfile.objects.get(user = user)
    con_dict = {'user':user,'user_profile':user_profile}
    return render_to_response('registration/index.html', con_dict, context)
