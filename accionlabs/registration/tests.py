from django.test.client import Client
from django.test import TestCase
from django.core.urlresolvers import reverse
from .models import UserProfile
from django.contrib.auth.models import User

# Create your tests here.


class ViewTests(TestCase):

	def setUp(self):
		self.client = Client()

	def test_register(self):
		response = self.client.get('/registration/register/')
		self.assertEqual(response.status_code,200)
		self.assertContains(response, 'Register with Accilon')
		response = self.client.post('/registration/register/',{'username':'jack','password':'jack','email':'a@a.net','website':'http://simon.net','twitter':'@simno'})
		self.assertEqual(response.status_code,200)
		self.assertContains(response,'thank you for registering')



	def test_login(self):
		response = self.client.get('/registration/login/')
		self.assertEqual(response.status_code,200)
		response1 = self.client.post('/registration/register/',{'username':'jack','password':'jack','email':'a@a.net','website':'http://simon.net','twitter':'@simno'})
		login = self.client.login(username='jack',password='jack')
		self.assertEqual(login,True)
		response = self.client.get(reverse('index'))
		self.assertEqual(response.status_code, 200)
		self.assertContains(response,'Name : jack')
 	

 	def test_loginfalse(self):
		response = self.client.get('/registration/login/')
		self.assertEqual(response.status_code,200)
		response1 = self.client.post('/registration/register/',{'username':'jack','password':'jack','email':'a@a.net','website':'http://simon.net','twitter':'@simno'})
		login = self.client.login(username='jack',password='jackwrong')
		self.assertEqual(login,False)


 	def test_logout(self):
		response = self.client.get('/registration/login/')
		self.assertEqual(response.status_code,200)
		response1 = self.client.post('/registration/register/',{'username':'jack','password':'jack','email':'a@a.net','website':'http://simon.net','twitter':'@simno'})
		self.client.login(username='jack',password='jack')
		self.client.logout()
		response=self.client.get(reverse('index'))
		self.assertEqual(response.status_code, 302)


	def test_profile(self):
		self.client.post('/registration/register/',{'username':'jack','password':'jack','email':'a@a.net','website':'http://simon.net','twitter':'@simno'})
		self.client.login(username='jack',password='jack')
		response = self.client.get(reverse('index'))
		self.assertEqual(response.status_code, 200)
		self.assertContains(response,'Name : jack')
		

class UserProfileTestCase(TestCase):
	def setUp(self):
		self.user1 = User.objects.create(username='jack',password='jack',email='jack@jack.net')
		UserProfile.objects.create(user=self.user1,website='http://simon.com',twitter='@simon')

	def test_userprofile(self):
		u = User.objects.get(username='jack')
		up = UserProfile.objects.get(user=u)
		self.assertEqual(u.username,'jack')
		self.assertEqual(u.password,'jack')
		self.assertEqual(u.email,'jack@jack.net')
		self.assertEqual(up.website,'http://simon.com')
		self.assertEqual(up.twitter,'@simon')




